const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');

const app = express();
app.use(bodyParser.json());

const sequelize = new Sequelize('c9','nechiulia','',{
  dialect : 'mysql',
  define : {
    timestamps : false
  }
});
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });
  


//Definire entitati

const User = sequelize.define('users',{
    email:{
        type : Sequelize.STRING,
        unique: true,
        allowNull : false,
        validate : {
            isEmail : true,
            isUnique: function (value, next) {
                    var self = this;
                    User.find({where: {email: value}})
                        .then(function (user) {
                            // reject if a different user wants to use the same email
                            if (user && self.id !== user.id) {
                                return next('Email already in use!');
                            }
                            return next();
                        })
                        .catch(function (err) {
                            return next(err);
                        });
                }
        }
    },
    password:{
        type : Sequelize.STRING,
        allowNull : false,
        validate : {
            len : [3,50]
        }
    }
    
}, {
  underscored : true
});

const Checkin= sequelize.define('checkins', {
    date_checkin:{
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false
    },
    car_id:{
       type: Sequelize.INTEGER,
        allowNull: false,
        validate:{isUnique: function (value, next) {
                    var self = this;
                    Checkin.find({where: {car_id: value}})
                        .then(function (checkin) {
                            // reject if a different checkin wants to use the same car id
                            if (checkin && self.id !== checkin.id) {
                                return next('This car already has a check-in');
                            }
                            return next();
                        })
                        .catch(function (err) {
                            return next(err);
                        });
                }
        }
    }
    
},  {
  underscored : true
});

const History= sequelize.define('historics', {
    registration_number:{
        type: Sequelize.STRING,
        allowNull: false,
        validate:{
            len:7
        }
    },
    date_checkin:{
        type: Sequelize.DATE,
        allowNull: false
    },
     id_parking:{
        type: Sequelize.INTEGER,
        allowNull : false,
        validate:{
            len : [1,50]
        }
    },
      date_checkout:{
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW,
        allowNull: false
    }
},  {
  underscored : true
});

const Car=sequelize.define('cars',{
    registration_number:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        validate:{
            len:7,
             isUnique: function (value, next) {
                    var self = this;
                    Car.find({where: {registration_number: value}})
                        .then(function (car) {
                            // reject if a different user wants to use the same registration
                            if (car && self.id !== car.id) {
                                return next('Registration number already in use!');
                            }
                            return next();
                        })
                        .catch(function (err) {
                            return next(err);
                        });
                }
        }
    },
    type_car:{
        type: Sequelize.STRING,
        allowNull:true
    }
    },{
        underscored:true
    });

const Parking = sequelize.define('parkings',{

    name_parking: {
        type:Sequelize.STRING,
        allowNull : false,
        validate: {
            len : [3,30]
        }
    },
    address: {
        type: Sequelize.STRING,
        allowNull: false,
        validate:{
            len : [3,50]
        }
    },
   available_spots:{
        type: Sequelize.INTEGER,
        allowNull:false,
    }
},{
    underscored:true
});


//Relatii intre entitati


User.hasMany(Car);

Parking.hasMany(Checkin);

app.get('/create', (req, res, next) => {
  sequelize.sync({force : true})
    .then(() => res.status(201).send('Database has been created'))
    .catch((err) => next(err))
});

//User 

//creare cont

 app.post('/register', (req, res, next) => {
   User.create(req.body)
     .then((users) => {
       res.status(200).send(users); 
     }, (err) =>{
       res.status(500).send(err);  
     });
 });
 
// autentificare

app.post('/login/:email/:password', (req, res) => {
   User.findOne({where:{email:req.params.email, password:req.params.password} })
    .then((user) => {
      if (user){
        res.status(200).json(user)
      }
      else{
        res.status(404).send('Sorry, we do not recognize this user')
      }
    }) 
});

//modificare parola
app.put('/users/changepass/:email', (req, res, next) => {
 User.findOne({where:{email:req.params.email}})
    .then((user) => {
      if (user){
        return user.update(req.body, {fields : ['password']})
      }
      else{
        res.status(404).send('No such user in our database')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('Your password  has been updated to our database')
      }
    })
    .catch((err) => next(err))
});  
//selectarea tuturor userilor

app.get('/get-all-users', (req,res) =>{
    User.findAll().then((users) =>{
        res.status(200).send(users);    
    });
    
})

//masini ale unui user
app.get('/cars/:id/user', (req, res, next) => {
  User.findById(req.params.id)
    .then((user) => {
      if (user){
       return user.getCars();
      }
      else{
        res.status(404).send('Sorry, we do not recognize this user')
    }})
    .then((cars)=>res.status(200).json(cars))
    .catch((err) => next(err))
})


//stergerea unei masini dupa nr de inmatriculare

app.delete('/cars/delete/:id',(req,res,next) =>{
    Car.findById(req.params.id)
    .then((cars) => {
        if(cars){
            return cars.destroy();
        }
        else{
            res.status(404).send('No such car in our database');
        }
        
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('The car is deleted');
        }
    })
    .catch((err) => next(err));
});


//Afisare tabela masini

app.get('/get-all-cars',(req,res) =>{
    Car.findAll().then((cars) =>{
        res.status(200).send(cars);
       
        
     
    })
});

//Adaugare masina de catre utilizator

app.post('/users/:email/cars', (req, res, next) => {
  let u;
  User.findOne({where:{email: req.params.email}})
    .then((user) => {
      if (user){
        u=user;
        return Car.create(req.body)
      }
      else{
        res.status(404).send('Sorry, we do not recognize this user')
    }})
    .then((car)=>{
      u.addCar(car)
      res.status(201).send('Your car has been added')
    })
    .catch((err) => next(err))
       
})


//Inserare in istoric

app.post('/addhistory/:id_checkin',(req,res) => {
        let h=  History.create()
    
        Checkin.findOne({where:{id: req.params.id_checkin}})
        .then((checkin)=>{
        if(checkin){
            Car.findOne({where:{id:checkin.car_id}})
            .then((car)=> {
        if(car){
        h.registration_number=car.registration_number
        h.date_checkin= checkin.date_checkin
        h.id_parking=checkin.parking_id
        History.create(h)
        
        res.status(201).send('The checkin was saved in history');}
        
            })
       
        }
         else {
            res.status(404).send('This checkin was not found')
        } })
        
    
})
 
//Afisare tabela istoric

  app.get('/show-History',(req,res)=>{
      History.findAll().then((historics)=>{
          res.status(200).send(historics);
      },(err) =>{
          res.status(500).send(err);
      });
  }); 


app.post('/add-parking',(req,res)=>{
    Parking.create(req.body).then((parkings)=>{
        res.status(200).send(parkings);
    },(err)=>{
        res.status(404).send(err);
    });
});

//Afisare tabela parcari

 app.get('/get-all-parkings',(req,res) => {
     Parking.findAll().then((parkings)=> {
         res.status(200).send(parkings);
     },(err) =>{
         res.status(404).send(err);
     });
 });
 

 
 app.get('/parkings/:address',(req,res)=>{
    Parking.findAll({where:{address: req.params.address}})
    .then((parkings)=>{
        res.status(200).send(parkings);
    },(err)=>{
        res.status(404).send(err);
    });
 });
 //update nr locuri parcare
app.put('/parkings/update-spots/:name_parking',(req,res,next) => {
    Parking.findOne({where:{name_parking:req.params.name_parking}})
    .then((parking)=>{
        if(parking){
            return parking.update(req.body, {fields: ['available_spots']})
        }
        else{
            res.status(404).send('No such parking in our database')
        }
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('Parking has been modified')
        }
    })
    .catch((err)=>next(err))
});

//afisare checkin dupa id

 app.get('/checkins/:id',(req,res)=>{
    Checkin.findAll({where:{id: req.params.id}}).
    then((checkins)=>{
        res.status(200).send(checkins);
    },(err) =>{
        res.status(404).send(err);
    });
});

//afisare toate checkin-urile curente
app.get('/get-all-checkins',(req,res)=>{
    Checkin.findAll().then((checkins)=>{
        res.status(200).send(checkins);
    },(err)=>{
        res.status(500).send("Server error");
    })
});

//afisare locuri disponibile ale unei parcari
app.get('/get-all-avalible_spots/:parkingname',(req,res)=>{
    Parking.findOne({where:{"name_parking":req.params.parkingname}})
    .then((parking)=>{
        let av_spots = parking.available_spots;
        res.status(200).send("No.spots:"+ av_spots);
    })
    .catch((err)=>{
        res.status(404).send("Parking doesn't exists");
    });
});

app.post('/checkins/parking/:parkingname/car/:carnumber', (req, res) => {
	Parking.findOne({where:{"name_parking": req.params.parkingname}})
    .then((parking) => {
      if (parking){
        let av_spots = parking.available_spots;
        let checkin= Checkin.create()
		checkin.parking_id = parking.id
		Car.findOne({where:{registration_number: req.params.carnumber}})
		.then((car)=> {
		 if(car){
		       Checkin.findOne({where:{car_id: car.id}})
		      .then((checkin1)=>{
		          if(checkin1){
		                     res.status(404).send('Sorry,one checkin already has this car')
		          }
		          else {
		                
		                if(av_spots>0){
		                parking.update({available_spots: av_spots-1})
		                checkin.car_id=car.id
		                Checkin.create(checkin)
		     		    res.status(201).send('Checkin was saved')
		                }
		                else{
		                    res.status(400).send('Sorry, the parking is full')
		                }
		          }
		      
		      
		      })
		 }
		 
		 else{
        res.status(404).send('Sorry, we do not recognize this car')
    }
		})
      }
      else{
        res.status(404).send('Sorry, we do not recognize this parking')
    }
        
    })
    
    .catch('server error')
})

/*app.post('/checkins/parking/:parkingaddress/car/:carnumber', (req, res) => {
	Parking.findOne({where:{"address": req.params.parkingaddress}})
    .then((parking) => {
      if (parking){
        let av_spots = parking.available_spots;  
        let checkin = req.body
		checkin.parking_id = parking.id
		Car.findOne({where:{registration_number: req.params.carnumber}})
		.then((car)=> {
		 if(car){
		    // if(av_spots>0){
		     parking.update({available_spots:av_spots-1})
		     checkin.car_id=car.id
		     Checkin.create(checkin)
		     res.status(200).send('The check in was saved')
		  //   }
		  //   else{
		  //       res.status(400).send('Sorry, the parking is full');
		  //   }
	
		 }   
		 else{ 
        res.status(404).send('Sorry, we do not recognize this car')
    }
		})
      }
      else{
        res.status(404).send('Sorry, we do not recognize this parking')
    }
        
    })
    
    .catch('server error')
})*/

app.delete('/checkin/delete/:carnumber',(req,res,next) =>{
	Car.findOne({where:{registration_number: req.params.carnumber}})
    .then((car) => {
        if(car){
            Checkin.findOne({where: {car_id: car.id}})
            .then((checkins) => {
        if(checkins){
              Parking.findOne({where:{id:checkins.parking_id}}).
              then((parking)=>{
                let av_spots = parking.available_spots;
                parking.update({available_spots:av_spots+1});
                return checkins.destroy();
            }).catch((err)=>{
                res.status(400).send('There s no such parking in our database')
            });
        }
        else{
            res.status(404).send('No such checkin in our database');
        }
    })
    .then(()=>{
        if(!res.headersSent){
               
            res.status(201).send('The checkin is deleted');
        }
    })
        }})
    .catch((err) => next(err));
});

app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
});